import { AppPage } from './app.po';
import * as _ from 'lodash';


export const All = '*';
export type AllType = typeof All;
export class WhiteProps {
  constructor(public path: string, public props: string[] | AllType | WhiteProps[]) {
  }
}
export class BlackProps {
  constructor(public path: string, public props: string[] | AllType) {
  }
}

export function cloneWithWhiteOrBlack(object: Object, props: string[] | AllType | (WhiteProps | BlackProps)[],
  path?: string, isWhite: boolean = true): any {

  if (_.isArray(object)) {
    throw new Error('Argument \'object\' must be only Object type.');
  }

  const obj = _.get(object, path);
  if (_.isArray(props) && ((props[0] instanceof WhiteProps) || (props[0] instanceof BlackProps))) {
    if (_.isArray(obj)) {
      const resArr = [];
      obj.forEach((item, index) => {
        props = props as (WhiteProps | BlackProps)[];
        const resObj = {};
        props.forEach(wb => {
          let objInPath = cloneWithWhiteOrBlack(item, wb.props, wb.path, wb instanceof WhiteProps);
          if (_.isObject(objInPath) && !_.isArray(objInPath)) {
            const previousObjInPaht = _.get(resObj, wb.path);
            objInPath = _.merge(previousObjInPaht, objInPath);
          }
          _.set(resObj, wb.path, objInPath);
        });
        resArr[index] = resObj;
      });
      return resArr;
    } else {
      props = props as (WhiteProps | BlackProps)[];
      const resObj = {};
      props.forEach(wb => {
        let objInPath = cloneWithWhiteOrBlack(path ? obj : object, wb.props, wb.path, wb instanceof WhiteProps);
        if (_.isObject(objInPath) && !_.isArray(objInPath)) {
          const previousObjInPaht = _.get(resObj, wb.path);
          objInPath = _.merge(previousObjInPaht, objInPath);
        }
        _.set(resObj, wb.path, objInPath);
      });
      return resObj;
    }
  } else if (_.isArray(props) && _.isString(props[0])) {
    if (_.isArray(obj)) {
      const resArr = [];
      obj.forEach((item, i) => {
        const objProps = isWhite ? _.pick(item, props) : _.omit(item, props);
        resArr[i] = _.cloneDeep(objProps);
      });
      return resArr;
    } else {
      const objProps = isWhite ? _.pick(obj, props) : _.omit(obj, props);
      return _.cloneDeep(objProps);
    }
  } else if (props === All) {
    if (path === undefined) {
      throw new Error('If \'All\' is specified should be path property defined.');
    }
    return isWhite ? (!_.isNil(obj) ? _.cloneDeep(obj) : obj) : undefined;
  }
}



describe('ivanov-test App', () => {
  const template = [
    new WhiteProps('grid.gridOptions', [
      new WhiteProps('columnDefs', [
        new BlackProps('state', ['prop1'])
      ]),
      new WhiteProps('rowDefs', All)
    ])
  ];
  const inputObj = {
    grid: {
      gridOptions: {
        columnDefs: [
          { state: 1, array: [1, 2, 3] },
          { state: { prop1: '', prop2: '', prop3: '', prop4: '' }, array: [4, 5, 6] },
          { state: { prop1: { subProp1: '' }, prop2: '', prop3: '' }, array: [4, 5, 6] },
          { state: { prop1: '', prop2: '' }, array: [4, 5, 6] },
          { state: 3, array: [7, 8, 9] }
        ],
        rowDefs: {
          count: 110,
          array: [1, 2, 3]
        },
        otherProperty: {}
      },
    }
  };

  const outputObj = {
    gridOptions: {
      columnDefs: [{}, { prop1: 'prop' }, {}]
    }
  };

  it('should be equal', () => {
    const clonedObj = cloneWithWhiteOrBlack(inputObj, template);
    const isEqual = _.isEqual(outputObj, clonedObj);
    // tslint:disable-next-line:no-debugger
    console.log(`see: ${JSON.stringify(clonedObj)}`);
    expect(isEqual).toBe(true);
  });
});
