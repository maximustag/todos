import { NgModule } from '@angular/core';
import { RouterModule, Routes, ActivatedRouteSnapshot, RouteReuseStrategy, DetachedRouteHandle } from '@angular/router';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoListResolver } from './todo-lists/todo-lists.resolver';
import { TodoListsComponent } from './todo-lists/todo-lists.component';

const routes: Routes = [
	{
		path: 'todolists', component: TodoListsComponent,
		children: [{ path: ':id', component: TodoListComponent, resolve: { todoList: TodoListResolver } }]
	},
	{ path: '', redirectTo: 'todolists', pathMatch: 'full' },
	{ path: '**', component: PageNotFoundComponent }
];


@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
