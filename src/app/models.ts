export interface IResponse{
	status: number;
	statusText: string;
}

export enum OkEnum {
	OK = 200,
	SERVER_ERROR = 500,
}


export class Base {
	id: number;
}
export class TodoList extends Base {
	title: string;
	items: TodoItem[];
}

export class TodoListVm extends TodoList {
	doneAll: string;
	constructor(private todoList: TodoList) {
		super();
		Object.assign(this, todoList);
		this.updateDoneAll();
	}

	public updateDoneAll() {
		this.doneAll = `${this.items.filter(i => i.status == TodoItemStatusEnum.DONE).length}/${this.items.length}`;
	}
}

export class TodoItem extends Base {
	title: string;
	status: TodoItemStatusEnum;
}

export enum TodoItemStatusEnum {
	UNDONE = 0,
	DONE = 1
}

export enum BaseSearchEnum {
	All = 2
}

export type SearchValues = TodoItemStatusEnum | BaseSearchEnum;

export function getId(items: Base[]) { return items.length === 0 ? 0 : items[items.length - 1].id + 1 }


