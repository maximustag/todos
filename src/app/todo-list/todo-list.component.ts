import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/Rx';
import { TodoList, TodoItem, TodoItemStatusEnum, BaseSearchEnum, SearchValues, getId, IResponse, OkEnum } from '../models';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { MatRadioButton } from '@angular/material/radio';
import { Subscription } from 'rxjs/Subscription';

@Component({
	selector: 'app-todo',
	templateUrl: './todo-list.component.html',
	styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit, OnDestroy {
	public id: number;
	public todoTitle: string;
	public todoList: TodoList;
	public filteredTodos: TodoItem[] = [];
	public todoItemStatusEnum = TodoItemStatusEnum;
	private paramsSub: Subscription;

	@Output() todoListChange = new EventEmitter<TodoList>();

	public search: string;
	public status: SearchValues = BaseSearchEnum.All;
	public search$: BehaviorSubject<string> = new BehaviorSubject<string>(null);
	public status$: BehaviorSubject<SearchValues> = new BehaviorSubject<SearchValues>(BaseSearchEnum.All);

	constructor(private http: HttpClient, private activatedRoute: ActivatedRoute) {
	}

	ngOnInit() {
		this.search$
			.debounceTime(400)
			.combineLatest(this.status$)
			.distinctUntilChanged()
			.subscribe(([search, status]) => {
				this.filteredTodos = this.todoList.items;
				if (search) {
					this.filteredTodos = this.filteredTodos.filter(i => i.title && i.title.toLowerCase().indexOf(search.toLowerCase()) !== -1);
				}
				if (status === TodoItemStatusEnum.UNDONE) {
					this.filteredTodos = this.filteredTodos.filter(i => i.status === TodoItemStatusEnum.UNDONE);
				}
			});

		this.paramsSub = this.activatedRoute.params.subscribe(params => {
			this.id = Number(params['id']);
			this.todoList = this.activatedRoute.snapshot.data.todoList;
			this.filteredTodos = this.todoList.items;
		});
	}

	addTodo() {
		if (!this.todoTitle) {
			return;
		}

		this.todoList.items.push({
			id: getId(this.todoList.items),
			title: this.todoTitle,
			status: TodoItemStatusEnum.UNDONE
		});

		this.todoTitle = null;

		this.http.post<IResponse>(`api/todolists/${this.id}`, this.todoList).subscribe(this.responseHandler);
	}

	deleteTodo(todoId: number) {
		this.todoList.items.splice(this.todoList.items.findIndex(i => i.id === todoId), 1);
		this.http.delete<IResponse>(`api/todolists/${this.id}/${todoId}`).subscribe(this.responseHandler);
	}

	markTodoAsDone(todoId: number) {
		const todo = this.todoList.items.find(i => i.id === todoId);
		todo.status = TodoItemStatusEnum.DONE;
		this.http.put<IResponse>(`api/todolists/${this.id}/${todoId}`, this.todoList).subscribe(this.responseHandler);
	}

	public onChangeStatus(btn: MatRadioButton) {
		this.status$.next(btn.value);
	}

	private emitTodoList() {
		this.todoListChange.emit(this.todoList);
	}

	private resetFilter() {
		this.search = '';
		this.status = BaseSearchEnum.All;
		this.search$.next('');
		this.status$.next(BaseSearchEnum.All);
	}

	private responseHandler = (res) => {
		if (res.status === OkEnum.OK) {
			console.log(res.statusText);
			this.resetFilter();
			this.emitTodoList();
		} else {
			console.error(res.statusText);
		}
	}

	ngOnDestroy() {
		if (this.paramsSub) {
			this.paramsSub.unsubscribe();
		}
	}
}
