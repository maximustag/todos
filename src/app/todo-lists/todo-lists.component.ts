import { Component, OnInit, OnDestroy } from '@angular/core';
import { TodoList, TodoItemStatusEnum, TodoItem, TodoListVm, getId, IResponse, OkEnum } from '../models';
import { FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TodoListComponent } from '../todo-list/todo-list.component';
import { Subscription } from 'rxjs/subscription';
import { ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-todo-lists',
	templateUrl: './todo-lists.component.html',
	styleUrls: ['./todo-lists.component.scss']
})
export class TodoListsComponent implements OnInit, OnDestroy {
	public todoLists: TodoListVm[] = [];
	public todoListTitle: string;
	public listItemStatus: typeof TodoItemStatusEnum = TodoItemStatusEnum;
	public todoListCtrl = new FormControl('', [Validators.required]);
	public currentTodoListId: number;

	private activeSub: Subscription;
	constructor(private http: HttpClient, private activatedRoute: ActivatedRoute) {
	}

	public ngOnInit() {
		this.http.get<TodoList[]>('api/todolists').subscribe(todoLists => {
			this.todoLists = todoLists ? todoLists.map(l => new TodoListVm(l)) : [];
		});
	}

	public getTodoListItemsByStatus = (list: TodoList, status: TodoItemStatusEnum): TodoItem[] => {
		return list.items.filter(i => i.status === status);
	}

	addTodoList() {
		if (!this.todoListCtrl.valid) {
			return;
		}

		this.todoLists.push(new TodoListVm({
			id: getId(this.todoLists),
			title: this.todoListCtrl.value,
			items: []
		}));

		this.http.post<IResponse>('api/todolists', this.todoLists).subscribe(res => {
			if (res.status === OkEnum.OK) {
				console.log(res.statusText);
				this.todoListCtrl.reset();
			} else {
				console.error(res.statusText);
			}
		});
	}

	public onActivate(activatedTodoList: TodoListComponent) {
		this.activeSub = activatedTodoList.todoListChange.filter(l => !!l).subscribe(changedTodoList => {
			const todoList = this.todoLists.find(l => l.id === activatedTodoList.id);
			Object.assign(todoList, changedTodoList);
			todoList.updateDoneAll();
		});
	}

	public ngOnDestroy() {
		if (this.activeSub) {
			this.activeSub.unsubscribe();
		}
	}
}


