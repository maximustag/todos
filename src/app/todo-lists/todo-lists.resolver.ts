import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/observable';
import { HttpClient } from '@angular/common/http';

import { TodoItem, TodoList } from '../models';

@Injectable()
export class TodoListResolver implements Resolve<TodoList> {

	constructor(private http: HttpClient) {
	}
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TodoList> {
		const id = route.params['id'];
		return this.http.get<TodoList>(`api/todolists/${id}`, {});
	}
}
