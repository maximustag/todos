import 'rxjs/add/operator/map';

import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as _ from 'lodash';

@Injectable()
export class HttpService {
	private baseUrl = 'api';

	constructor(private http: HttpClient) {
	}

	public get<T>(url: string, options?: IHttpOptions): Observable<T> {
		return this.http.get<T>(`${this.baseUrl}${url}`, this.prepareOptions(options));
	}

	public post<T>(url: string, postData: any, options?: IHttpOptions): Observable<T> {
		return this.http.post<T>(this.baseUrl, JSON.stringify(postData), this.prepareOptions(options));
	}

	private prepareOptions(httpOptions: IHttpOptions): IHttpClientOptions {
		if (_.isNil(httpOptions)) {
			return undefined;
		}

		const httpClientOptions: IHttpClientOptions = {};

		if (httpOptions.params) {
			let httpParams = new HttpParams();
			httpOptions.params.forEach(tuple => {
				httpParams = httpParams.append(tuple[0], tuple[1] ? tuple[1].toString() : null);
			});
			httpClientOptions.params = httpParams;
		}

		if (httpOptions.headers) {
			let httpHeaders = new HttpHeaders();
			httpOptions.headers.forEach(tuple => {
				httpHeaders = httpHeaders.append(tuple[0], tuple[1]);
			});
			httpClientOptions.headers = httpHeaders;
		}

		if (httpOptions.withCredentials) {
			httpClientOptions.withCredentials = httpOptions.withCredentials;
		}

		return httpClientOptions;
	}
}

export interface IHttpOptions {
	headers?: [string, (string | string[])][];
	params?: [string, string | number][];
	withCredentials?: boolean;
}

export interface IHttpClientOptions {
	headers?: HttpHeaders;
	params?: HttpParams;
	withCredentials?: boolean;
}
