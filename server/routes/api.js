const express = require('express');
const router = express.Router();
const fs = require('fs');

router.get('/todolists', (req, res) => {
  fs.readFile('./server/todolists.json', function (err, data) {
    if (err) {
      console.log(err);
      return;
    }
    res.send(data);
  });
});

router.post('/todolists', (req, res) => {
  fs.writeFile('./server/todolists.json', JSON.stringify(req.body), function (err) {
    if (err) {
      res.status(500).send({ status: 500, statusText: 'Todo lists not saved' });;
      return;
    }
    res.status(200).send({ status: 200, statusText: 'Todo lists saved' });
  });
});

router.get('/todolists/:id', (req, res) => {
  fs.readFile('./server/todolists.json', function (err, data) {
    if (err) {
      console.log(err);
      return;
    }
    if (!data) {
      return;
    }
    todolists = JSON.parse(data);
    const id = req.params['id'];
    const todolist = todolists.find(l => l.id == id);
    res.send(todolist);
  });
});

router.post('/todolists/:id', (req, res) => {
  fs.readFile('./server/todolists.json', function (err, data) {
    if (err) {
      console.log(err);
      return;
    }
    todolists = JSON.parse(data);
    const id = req.params['id'];
    let todolist = todolists.find(l => l.id == id);
    Object.assign(todolist, req.body);

    fs.writeFile('./server/todolists.json', JSON.stringify(todolists), function (err) {
      if (err) {
        res.status(500).send({ status: 500, statusText: 'Todo list items not saved' });;
        return;
      }
      res.status(200).send({ status: 200, statusText: 'Todo list items saved' });
    });
  });
});

router.delete('/todolists/:id/:todoId', (req, res) => {
  fs.readFile('./server/todolists.json', function (err, data) {
    if (err) {
      console.log(err);
      return;
    }
    todolists = JSON.parse(data);
    const id = req.params['id'];
    const todoId = req.params['todoId'];

    let todolist = todolists.find(l => l.id == id);
    todolist.items.splice(todolist.items.findIndex(i => i.id == todoId), 1)

    fs.writeFile('./server/todolists.json', JSON.stringify(todolists), function (err) {
      if (err) {
        res.status(500).send({ status: 500, statusText: 'Todo is not deleted from list items' });;
        return;
      }
      res.status(200).send({ status: 200, statusText: 'Todo deleted from list items' });
    });
  });
});

router.put('/todolists/:id/:todoId', (req, res) => {
  fs.readFile('./server/todolists.json', function (err, data) {
    if (err) {
      console.log(err);
      return;
    }
    todolists = JSON.parse(data);
    const id = req.params['id'];
    let todolist = todolists.find(l => l.id == id);
    Object.assign(todolist, req.body);

    fs.writeFile('./server/todolists.json', JSON.stringify(todolists), function (err) {
      if (err) {
        res.status(500).send({ status: 500, statusText: 'Todo is not updated from list items' });;
        return;
      }
      res.status(200).send({ status: 200, statusText: 'Todo updated from list items' });
    });
  });
});

module.exports = router;
